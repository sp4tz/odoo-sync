<?php

/**
 * Created by Magneticlab.
 * User: david
 * Date: 04.05.2016
 * Time: 10:11
 */

class Odoo_Webservices
{
    private $url = 'localhost';
    private $db = 'mlab';
    private $username = 'admin';
    private $password = 'admin';
    private $uid = null;
    private $ripcord_models = null;

    function __construct()
    {
        require_once( 'ripcord/ripcord.php' );
        $common = ripcord::client( "$this->url/xmlrpc/2/common" );
        $uid = $common->authenticate($this->db, $this->username, $this->password, array());
        $this->uid = $uid;
        $this->ripcord_models = ripcord::client("$this->url/xmlrpc/2/object");
    }

    public function list_product($model){
        $rpc = ripcord::client("$this->url/xmlrpc/2/object");
        return $rpc->execute_kw($this->db, $this->uid, $this->password,
            $model, 'search_read', array(
                array(
                    // array('default_code', '=', 'INTL0510'),
                ),
            ), array(
                'fields' => array('public_categ_ids')
            ));


    }

    public function create_product($model, $fields)
    {
        return $this->ripcord_models->execute_kw($this->db, $this->uid, $this->password, $model, 'create', array( $fields ));
    }
    
}