<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Odoo</title>
</head>
<pre>
<?php
/**
 * Created by magneticlab.
 * User: david
 * Date: 04.05.2016
 * Time: 10:24
 */
include_once ('Odoo_Webservices.php');

$odoo = new Odoo_Webservices();

// Récupération de la liste de produits du catalogue ODOO
$odoo_catalogue = $odoo->list_product('product.product');
$odoo_articles = array();
// var_dump($odoo_catalogue); return false;

// Nouveau produits
$fields = array(
    'name' => 'produit 1',
    'default_code' => '1111',
    'type' => 'product',
    'description' => 'test importation',
    'categ_id' => 81,
    'public_categ_ids' => array(   6,0, array( 81 ) )
);
$odoo->create_product('product.product', $fields);
?>
</html>
